<?php

namespace multefront\controllers;

use Yii;
use app\models\Category;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Products;
use multebox\models\Banks;
use multebox\models\PaymentPlanTemplate;
use multebox\models\PaymentPlanTemplateDetail;
use multebox\models\Inventory;
class IjaxController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }



    public function actionGetCategory()
    {
        $id = $_GET['id'];
        $category_json = [];
        if ($id == '#')
            $category_loop = Category::find()->where(['parent_id'=>null,'status'=>'active'])->all();
        else
            $category_loop = Category::find()->where(['parent_id'=>$id,'status'=>'active'])->all();

        foreach ($category_loop as $key => $value) {
            # code...
            $resp['id'] = "".$value->id;
            $resp['text'] = $value->name;
            $check_child = Category::find()->where(['parent_id'=>$value->id,'status'=>'active'])->count();
            if($check_child > 0)
                $resp['children'] = true;
            else
                $resp['children'] = false;
            $category_json[] = $resp;
        }
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        $response->data = $category_json;
        return $response;

    }

    public  function actionGetProduct(){

        $id = $_REQUEST['id'];
        $data = \app\models\Products::find()->where('id='.$id)->one();
        $resp['id'] = $data->id;
        $resp['name'] = $data->name;
        $resp['price'] = $data->price;
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        $response->data = $resp;

    }
    public function actionGetPlans(){
        $bank_id = $_REQUEST['bank_id'];
        $q = PaymentPlanTemplate::find()->where(['=','bank_id',$bank_id])->all();
        /*echo '<pre>';
        echo  print_r($q);
        echo '</pre>';exit;*/
        $count=0;
        foreach($q as $val ){
            $plan['id'.$count] = $val->id;
            $plan['plan'.$count] = $val->plan_name;
            $count++;
        }
        $plan['count']= $count;

        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        $response->data = $plan;


    }
     public function actionGetInstallments(){

        $price_id = $_REQUEST['price'];
        $price = Inventory::find()->where(['=','product_id',$price_id])->one()->price;
         $id = $_REQUEST['id'];
         $query = PaymentPlanTemplateDetail::find()->where(['=','payment_plan_template_id',$id])->orderBy(['id'=>'ASC'])->all();
        /* echo '<pre>';
         echo  print_r($query);
         echo '</pre>';*/
         $count = 0;
         $total =0;

             foreach ($query as $val){

                if($val->format == 'rs'){
                    if($val->type == 'initial'){

                        $installment['installment0'] = $val->payment_amount;
                        $total +=  $val->payment_amount;
                    }
                    else
                    {
                        $installment['installment'.$count]  = $val->payment_amount;
                        $total +=  $val->payment_amount;
                    }
                    $installment['total'] = $total;
                    $response = Yii::$app->response;
                    $response->format = \yii\web\Response::FORMAT_JSON;
                    $response->data = $installment;

                }
                else{
                    if($val->type == 'initial'){
                        $installment['installment0']  = $price *$val->payment_amount /100;
                        $total +=  $installment['installment0'];
                    }
                    else{
                        $installment['installment'.$count]  = $price * $val->payment_amount /100;
                        $total += $installment['installment'.$count];
                    }
                    $installment['total'] = $total;
                    $response = Yii::$app->response;
                    $response->format = \yii\web\Response::FORMAT_JSON;
                    $response->data = $installment;
                }
                 /*$resp['type'.$count]= $val->type;
                 $resp['format'.$count]= $val->format;
                 $resp['amount'.$count] =  $val->payment_amount;
                 $response = Yii::$app->response;
                 $response->format = \yii\web\Response::FORMAT_JSON;
                 $response->data = $resp;*/
                 $count++;

             }
    }

    public function actionGetbank()
    {
       // $this->enableCsrfValidation = false;
        $id = $_REQUEST['id'];

            $query = Banks::find()->all();

            $count=0;
            foreach($query as $val ){
                $bank['id'.$count] = $val->id;
                $bank['bank'.$count] = $val->name;
                $count++;
            }
            $bank['count']= $count;

            $response = Yii::$app->response;
            $response->format = \yii\web\Response::FORMAT_JSON;
            $response->data = $bank;
             /*echo '<pre>';
            echo  print_r($query);
            echo '</pre>';*/
    }
}