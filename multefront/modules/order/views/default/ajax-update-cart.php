<?php
use multebox\models\Inventory;
use multebox\models\File;
use yii\helpers\Url;
?>

<input type="hidden" class="hiddencartvalue" value="<?=$total_items?>">
<input type="hidden" class="hiddenremainingstock" value="<?=$remaining_stock?>">

<?php
foreach($cart_items as $cart)
{
	$inventory_item = Inventory::findOne($cart->inventory_id);
	$prod_title = $inventory_item->product_name;
	$fileDetails = File::find()->where("entity_type='product' and entity_id=".$inventory_item->product_id)->one();
?>
  <tr>
	<td class="text-center"><a href="<?=Url::to(['/product/default/detail', 'inventory_id' => $cart->inventory_id])?>"><img class="img-thumbnail" title="<?=$prod_title?>" alt="<?=$prod_title?>" src="<?=Url::base()?>/../../multeback/web/attachments/<?=$fileDetails->id?><?=strrchr($fileDetails->file_name, ".")?>"></a></td>
	<td class="text-left"><a href="<?=Url::to(['/product/default/detail', 'inventory_id' => $cart->inventory_id])?>"><?=$prod_title?></a></td>
	<td class="text-right">x <?=$cart->total_items?></td>
	<td class="text-center"></td>
  </tr>
<?php
}
?>