<?php
use multebox\models\File;
use multebox\models\Product;
use multebox\models\ProductCategory;
use multebox\models\ProductSubCategory;
use multebox\models\ProductSubSubCategory;
use multebox\models\ProductBrand;
use multebox\models\ProductAttributes;
use multebox\models\ProductAttributeValues;
use multebox\models\InventoryDetails;
use multebox\models\Inventory;
use multebox\models\Vendor;
use multebox\models\search\MulteModel;
use multebox\models\ProductReview;
use multebox\models\User;
use yii\helpers\Url;
use yii\helpers\Html;


include_once('../web/cart_script.php');

$product = Product::findOne($inventory->product_id);
$fileDetails = File::find()->where("entity_type='product' and entity_id='$inventory->product_id'")->orderBy("id asc")->all();
$product_reviews = ProductReview::find()->where("product_id=".$inventory->product_id)->all();
?>
<style>
    .row.product-info {
        background-color: whitesmoke;
    }
    #content {
               background-color: whitesmoke;
    }
    .product-info .cart #button-cart {
        padding: 2px 15px;
        text-transform: uppercase;
    }
</style>
<script type="text/javascript" src="<?=Url::base()?>/js/jquery-2.1.1.min.js"></script>
<script>
$(document).ready(function()
{
	$('.addtocart2').on('click', function () {
	    var pay_method = $('#payment-method').val();
	    var bank_id  = $('#bank').val();
        var plan_id  = $('#plan').val();

        var cart = $('.mycart');
        var imgtodrag = $('.image').find("img").eq(0);
        if (imgtodrag) {
            var imgclone = imgtodrag.clone()
                .offset({
                top: imgtodrag.offset().top,
                left: imgtodrag.offset().left
            })
                .css({
                'opacity': '0.5',
                    'position': 'absolute',
                    'height': '150px',
                    'width': '150px',
                    'z-index': '100'
            })
                .appendTo($('body'))
                .animate({
                'top': cart.offset().top + 10,
                    'left': cart.offset().left + 10,
                    'width': 75,
                    'height': 75
            }, 1000, 'easeInOutExpo');
            
            imgclone.animate({
                'width': 0,
                    'height': 0
            }, function () {
                $(this).detach()
            });
        }

		$.post("<?=Url::to(['/order/default/ajax-add-to-cart'])?>", { 'inventory_id': $(this).val(), 'total_items' : $('#input-quantity').val(),'payment_method' : pay_method, 'bank_id' : bank_id, 'plan_id' : plan_id,'_csrf' : '<?=Yii::$app->request->csrfToken?>'}) .done(function(result){
					
					$('.carttable tbody').html(result);
					$('.cartcount').html($('.hiddencartvalue').val() + ' item(s)');
					$('.remainingstock').val($('.hiddenremainingstock').val());

					$('.confirmmodal').modal('show');
					setTimeout(function() {$('.confirmmodal').modal('hide');}, 1500);
				})

		//$.get("<?=Url::to(['/product/default/detail'])?>", { 'inventory_id': $(this).val()}) .done(function(data){ $( "body" ).html(data);});

	    });

	//var error='';
	$(".plus").click(function(event)
	{
		//var stock = <?=$inventory->stock?>;
		var stock = $('.remainingstock').val();
		//Remove_Error($(this));
		if(stock < parseInt($('#input-quantity').val()))
		{
			//alert($('#input-quantity').val());
			//error+=Add_Error($(this),'<?=Yii::t('app','Not enough stock!')?>');
			$('#input-quantity').val(stock);
			//event.preventDefault();
			return false;
		}
	});

	$('#input-quantity').change(function(event)
	{
		//var stock = <?=$inventory->stock?>;
		var stock = $('.remainingstock').val();
		//Remove_Error($(this));
		if(stock < parseInt($('#input-quantity').val()))
		{
			//alert("Not enough stock!");
			//error+=Add_Error($(this),'<?=Yii::t('app','Not enough stock!')?>');
			$('#input-quantity').val(stock);
			//event.preventDefault();
			return false;
		}
	});
	
	//Remove_Error($(this));

	// Elevate Zoom for Product Page image
$("#zoom_01").elevateZoom({
	gallery:'gallery_01',
	cursor: 'pointer',
	galleryActiveClass: 'active',
	imageCrossfade: true,
	zoomWindowFadeIn: 500,
	zoomWindowFadeOut: 500,
	lensFadeIn: 500,
	lensFadeOut: 500,
	loadingIcon: '<?=Url::base()?>/image/progress.gif'
	}); 
//////pass the images to swipebox
$("#zoom_01").bind("click", function(e) {
  var ez = $('#zoom_01').data('elevateZoom');
	$.swipebox(ez.getGalleryList());
  return false;
});

});
</script>
<input type="hidden" class="remainingstock" value="<?=$inventory->stock?>">
<div class="container">
<div id="container">
    
      <!-- Breadcrumb Start-->
      <ul class="breadcrumb">
	  <?php
	    $url1 = Url::to(['/site/index']);
		$url2 = Url::to(['/product/default/listing', 'category_id' => $product->category_id]);
		$url3 = Url::to(['/product/default/listing', 'category_id' => $product->category_id, 'sub_category_id' => $product->sub_category_id]);
		$url4 = Url::to(['/product/default/listing', 'category_id' => $product->category_id, 'sub_category_id' => $product->sub_category_id, 'sub_subcategory_id' => $product->sub_subcategory_id]);
	  ?>
	    <li><a href="<?=$url1?>"><i class="fa fa-home"></i></a></li>
        <li><a href="<?=$url2?>"><?=ProductCategory::findOne($product->category_id)->name?></a></li>
		<li><a href="<?=$url3?>"><?=ProductSubCategory::findOne($product->sub_category_id)->name?></a></li>
		<li><a href="<?=$url4?>"><?=ProductSubSubCategory::findOne($product->sub_subcategory_id)->name?></a></li>
      </ul>

      <!-- Breadcrumb End-->
      <div class="row">
		 <div id="content" class="col-sm-12">
          <div itemscope itemtype="">
		  <?php
		  if($inventory->product->digital)
		  {
		  ?>
            <h1 class="title" itemprop="name"><?=$inventory->product_name?> <small>(<?=Yii::t('app', 'Digital Product')?>)</small></h1>
		  <?php
		  }
		  else
		  {
		  ?>
			<h1 class="title" itemprop="name"><?=$inventory->product_name?></h1>
		  <?php
		  }
		  ?>

            <div class="row product-info">
              <div class="col-sm-6">
                <div class="image">
				  <img class="img-responsive img-mydetail" itemprop="image" id="zoom_01" src="<?=Url::base()?>/../../multeback/web/attachments/<?=$fileDetails[0]->id?><?=strrchr($fileDetails[0]->file_name, ".")?>" title="<?=$fileDetails[0]->file_title?>" alt="<?=$fileDetails[0]->file_title?>" data-zoom-image="<?=Url::base()?>/../../multeback/web/attachments/<?=$fileDetails[0]->id?><?=strrchr($fileDetails[0]->file_name, ".")?>" /> 
				</div>
                <div class="center-block text-center"><span class="zoom-gallery"><i class="fa fa-search"></i> <?=Yii::t('app', 'Click image for full sized images')?></span></div>
                <div class="image-additional" id="gallery_01"> 
				<?php
				foreach($fileDetails as $file)
				{
				?>
				  <a class="thumbnail" href="#" data-zoom-image="<?=Url::base()?>/../../multeback/web/attachments/<?=$file->id?><?=strrchr($file->file_name, ".")?>" data-image="<?=Url::base()?>/../../multeback/web/attachments/<?=$file->id?><?=strrchr($file->file_name, ".")?>" title="<?=$file->file_title?>"> <img src="<?=Url::base()?>/../../multeback/web/attachments/<?=$file->id?><?=strrchr($file->file_name, ".")?>" title="<?=$file->file_title?>" alt = "<?=$file->file_title?>" class="img-thumbnail-list"/></a> 
				<?php
				}
				?>
				</div>
              </div>
              <div class="col-sm-6">
                <ul class="list-unstyled description">
				<div class="row">
			    <div class="col-sm-12">
				<?php
				/*if($product->brand_id != '')
				{
				*/?><!--
                  <li><b><?/*=Yii::t('app', 'Brand')*/?>:</b> <a href="#"><span itemprop="brand"><?/*=ProductBrand::findOne($product->brand_id)->name*/?> </span></a></li>
				<?php
/*				}
				*/?>
                  <li><b><?/*=Yii::t('app', 'Product Code')*/?>:</b> <span itemprop="mpn"><?/*='INVT'.str_pad($product->id, 9, "0", STR_PAD_LEFT)*/?></span></li>
				<?php
/*				if($inventory->stock > 0)
				{
				*/?>
                  <li><b><?/*=Yii::t('app', 'Availability')*/?>:</b> <span class="instock"><?/*=Yii::t('app', 'In Stock')*/?></span></li>
				<?php
/*				}
				else
				{
				*/?>
				  <li><b><?/*=Yii::t('app', 'Availability')*/?>:</b> <span class="nostock"><?/*=Yii::t('app', 'Out of Stock')*/?></span></li>
				--><?php
/*				}*/
				
				?>
				</div>
				</div>
				<!--<div class="row">
				<div class="col-sm-12">
				<li><b><?php /*Yii::t('app', 'Sold By')*/?>:</b> <span itemprop="mpn">
				<?php /*Html::a(Vendor::findOne($inventory->vendor_id)->vendor_name, Url::to(['/product/default/filter']), [
																'data'=>[
																	'method' => 'post',
																	'params'=>['vendor_id'=> $inventory->vendor_id],
																],
																'data-toggle' => 'tooltip',
																'title' => Yii::t('app', 'Click To View All Products By This Seller'),
															]) */?>
				</span></li>

				</div>
				</div>-->

				<!--<div class="row">
				<div class="col-sm-4">
				<li><b><?php /*Yii::t('app', 'Seller Rating')*/?>:</b></li>
				</div>

				<div class="col-sm-8 text-left">
				<li><input type="text" class="multe-rating-nocap-sm" value="<?php /*$inventory->vendor_rating*/?>" readonly></li>
				</div>
				</div>-->

				<div class="row">
				<div class="col-sm-12">
				<?php
				$inventoryPrice = floatval($inventory->price);
				if($inventory->price_type == 'B')
				{
				  foreach(json_decode($inventory->attribute_price) as $row)
				  {
					  $inventoryPrice += floatval($row);
				  }
				}

				if($inventory->discount_type == 'P')
				$inventoryDiscount = $inventory->discount;
				else
				{
					if($inventoryPrice > 0)
						$inventoryDiscount = round((floatval($inventory->discount)/$inventoryPrice)*100,2);
					else
						$inventoryDiscount = 0;
				}

				$inventoryDiscountedPrice = round($inventoryPrice - $inventoryPrice*$inventoryDiscount/100, 2);
				?>
				</div>
				</div>
                </ul>
				
                <ul class="price-box">
                  <li class="price" itemprop="offers" itemscope itemtype="">
				  <?php
				  if ($inventoryPrice != $inventoryDiscountedPrice)
				  {
				  ?>
				  <span class="price-old"><?=MulteModel::formatAmount($inventoryPrice)?></span>
				  <?php
				  }
				  ?>
				  <span itemprop="price"><?=MulteModel::formatAmount($inventoryDiscountedPrice)?></span></li>
                  <li></li>
				  <?php
				  if ($inventoryPrice != $inventoryDiscountedPrice)
				  {
				  ?>
                  <li><?=Yii::t('app', 'Discount')?>: <?=$inventoryDiscount?>%</li>
				  <?php
				  }
				  ?>
                </ul>
                <div id="product">
				  <form method="post" name="" action=""  enctype="multipart/form-data">
				  <?php Yii::$app->request->enableCsrfValidation = true; ?>
				  <input type="hidden" name="_csrf" value="<?php echo $this->renderDynamic('return Yii::$app->request->csrfToken;'); ?>">
				  <input type="hidden" name="product_id" value="<?=$inventory->product_id?>">
				  <?php
				  if ($inventory->slab_discount_ind == 1)
				  {
				  ?>
				  <?=Yii::t('app', 'Bulk Discount')?>:
				  <div class="table-responsive">
					<table class="table table-bordered">
					  <tbody>
					    <tr>
						  <th><?=Yii::t('app', 'Minimum Quantity')?></th>
						  <?=$inventory->slab_1_range > 0?'<td>'.$inventory->slab_1_range.'</td>':''?>
						  <?=$inventory->slab_2_range > 0?'<td>'.$inventory->slab_2_range.'</td>':''?>
						  <?=$inventory->slab_3_range > 0?'<td>'.$inventory->slab_3_range.'</td>':''?>
						  <?=$inventory->slab_4_range > 0?'<td>'.$inventory->slab_4_range.'</td>':''?>
						</tr>
						<tr>
						  <th><?=Yii::t('app', 'Discount')?></th>
						  <?=$inventory->slab_1_range > 0?'<td>'.($inventory->slab_discount_type=='F'?MulteModel::formatAmount($inventory->slab_1_discount):$inventory->slab_1_discount.'%').'</td>':''?>
						  <?=$inventory->slab_2_range > 0?'<td>'.($inventory->slab_discount_type=='F'?MulteModel::formatAmount($inventory->slab_2_discount):$inventory->slab_2_discount.'%').'</td>':''?>
						  <?=$inventory->slab_3_range > 0?'<td>'.($inventory->slab_discount_type=='F'?MulteModel::formatAmount($inventory->slab_3_discount):$inventory->slab_3_discount.'%').'</td>':''?>
						  <?=$inventory->slab_4_range > 0?'<td>'.($inventory->slab_discount_type=='F'?MulteModel::formatAmount($inventory->slab_4_discount):$inventory->slab_4_discount.'%').'</td>':''?>
						</tr>
					  </tbody>
					</table>
				  </div>
				  <?php
				  }
				  ?>
                  
				  
				  <?php
				  $productAttributesList = ProductAttributes::find()->where('parent_id='.ProductSubSubCategory::findOne($product->sub_subcategory_id)->id.' and fixed=1 and active=1')->orderBy('id')->all();

				  if($productAttributesList)
				  {
					  ?>
					  <h3 class="subtitle"><?=Yii::t('app', 'Available Options')?></h3>
					  <?php
				  }
					
				  $i=0;
				  foreach($productAttributesList as $productAttributes)
				  {
					  $productAttributeValueList = ProductAttributeValues::findOne($productAttributes->fixed_id);
				  ?>
                  <div class="form-group required">
				    <!--<input type="hidden" name="attribute_ids[]" value="<?=$productAttributes->id?>">-->
				    <input type="hidden" name="attribute_names[]" value="<?=$productAttributeValueList->name?>">
                    <label class="control-label"><?=$productAttributeValueList->name?></label>
                    <select class="form-control" id="input-option" name="attribute_value[]">
                      <option value=""> --- <?=Yii::t('app', 'Please Select')?> --- </option>
					  <?php
					  foreach(json_decode($productAttributeValueList->values) as $value)
					  {
					  ?>
                      <option value="<?=$value?>" <?=(json_decode($inventory->attribute_values))[$i] == $value?'selected':''?> data-validation="required" mandatory-field><?=$value?> </option>
					  <?php
					  }
					  ?>
                    </select>
                  </div>
				  <?php
				  $i++;
				  }

				  if($i > 0)
				  {
				  ?>
				  <div>
				  <button type="submit" id="button-cart" class="btn btn-success btn-block ashish"><?=Yii::t('app', 'Find items with applied filter')?></button>
				  </div><br/>
				  <?php
				  }
				  ?>
				  </form>
                    <!--<div class="row col-sm-12">
                        <div class="col-sm-2 text-center">
                            <strong>Product Rating:</strong>
                        </div>
                        <div class="col-sm-2 text-center">
                            <input type="text" class="multe-rating-nocap-sm" value="<?/*=$inventory->product_rating*/?>" readonly>
                        </div>
                        <div class="col-sm-3 text-center">
                            <?/*=count($product_reviews)*/?> <?/*=Yii::t('app', 'customer review(s)')*/?>
                        </div>
                    </div>-->
                    <div class="content-product-right  col-md-10 col-sm-12 col-xs-12">


                        <!--Advance & Installment Plan Options -->
                        <div id="product">
                            <input type="hidden" name="advance_price" id="advance" value="">
                            <input type="hidden" name="remaining" id="remaining">
                            <input type="hidden" name="monthly_ins" id="monthly_ins" value="">
                            <input type="hidden" name="total_amount" id="total_amount" value="">
                            <input type="hidden" name="province_name" id="province_name" value="">
                            <input type="hidden" name="product_price" id="product_price" value="">
                            <input type="hidden" name="adv_plan" id="adv_plan" value="">
                            <input type="hidden" name="monthly_plan" id="monthly_plan" value="">
                            <input type="hidden" name="payment_plan" id="payment_plan" value="">

                            <!--Province-->


                            <div style="font-size:14px;font-weight:600;padding-top:10px;" class="control-label">Select Your Province:</div>
                            <div>
                                <select name="province_id" id="province_id" class="form-control required">
                                    <option value="1" selected="selected">Punjab</option>
                                    <option value="2">Sindh</option>
                                    <option value="3">Khyber Pakhtunkhwa</option>
                                    <option value="4">Balochistan</option>
                                </select>

                            </div>

                            <div style="font-size:14px;font-weight:600;padding-top:10px;" class="control-label">Select Your Payment Process:</div>
                            <div>
                                <div class="form-group required">
                                    <select name="payment_method" id="payment-method" class="form-control" onchange="getbanks(this)">
                                        <option value="2" selected="selected">Full Cash Payment</option>
                                        <option value="1" >Installments and Insurance</option>


                                    </select>
                                </div>
                            </div>
                            <div class="banks" id="banks">
                            <!--<div>
                                <div class="form-group required">
                                    <select name="payment_method" id="input-payment-method" class="form-control" onchange="selMthd(this)">
                                        <option value="1" selected="selected">Bank Alfhalah</option>
                                        <option value="2">HBL</option>
                                        <option value="2">Meezan Bank</option>
                                        <option value="2">UBL</option>
                                        <option value="2">Silk Bank</option>
                                        <option value="2">Albaraka</option>

                                    </select>
                                </div>
                            </div>-->
                            </div>




                            <!--<div id="advance-section" class="form-group required" style="display:block;">
                                <label class="control-label" for="input-option-lbl-advance">Advance</label>
                                <select onchange="selAdv(this)" name="advance" id="input-option-advance" class="form-control">

                                    <option value=""> --- Please Select --- </option>
                                    <option value="30">30%</option>
                                    <option value="1">1st installment</option>

                                </select>
                            </div>-->


                            <!-- Installment plan-->
                            <div id="plans" class="form-group required" >

                                <!--<select onchange="selPlan(this)" name="installment_plan" id="input-option-plan" class="form-control">

                                    <option value=""> --- Please Select --- </option>
                                    <option value="12">6 Months</option>
                                    <option value="21">9 Months</option>
                                    <option value="24">12 Months</option>
                                </select>-->
                            </div>


                            <!--Advance & Installment Plan Options -->



                            <div id="showCalculator" class="row" style="margin-bottom:7px;"></div>


                        </div>

                        <div class="cart">
                            <div>
                                <div class="qty">
                                    <label class="control-label" for="input-quantity" ><?=Yii::t('app', 'QUANTITY')?></label>
                                    <input type="text" name="quantity" value="1" size="2" id="input-quantity" class="form-control" disable/>
                                    <a class="qtyBtn plus" href="javascript:void(0);">+</a><br />
                                    <a class="qtyBtn minus" href="javascript:void(0);">-</a>
                                    <div class="clear"></div>
                                </div>
                                <?php
                                if($inventory->stock > 0)
                                {
                                    ?>
                                    <button id="button-cart" style="height: 41px;" class="btn btn-primary btn-lg addtocart2" title="Add to Cart" type="button" value="<?=$inventory->id?>"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                                    <!--<button </i> type="button" id="button-cart" style="height: 41px;" class="btn btn-primary btn-lg addtocart2" value="<?/*=$inventory->id*/?>"><?/*=Yii::t('app', 'Add To Cart')*/?></button>-->
                                    <?php
                                }
                                else
                                {
                                    ?>
                                    <button type="button" id="button-cart" style="height: 41px;" class="btn btn-primary btn-lg disabled"><?=Yii::t('app', 'Out of Stock')?></button>
                                    <?php
                                }
                                ?>
                            </div>

                        </div>
                </div>
               
              </div>
            </div>
            </div>
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab-description" data-toggle="tab"><?=Yii::t('app', 'Description')?></a></li>
              <li><a href="#tab-specification" data-toggle="tab"><?=Yii::t('app', 'Specification')?></a></li>
              <li><a href="#tab-review" data-toggle="tab"><?=Yii::t('app', 'Reviews')?> (<?=count($product_reviews)?>)</a></li>
            </ul>
            <div class="tab-content">
              <div itemprop="description" id="tab-description" class="tab-pane active">
                <?=$product->description?>
              </div>
              <div id="tab-specification" class="tab-pane">
			  <?php
			  $inventoryDetails = InventoryDetails::find()->where('inventory_id='.$inventory->id)->all();
			  foreach($inventoryDetails as $inventoryDetail)
			  {
			  ?>
                <table class="table table-bordered">
                  <tbody>
                    <tr>
                      <td width=50%><strong><?=ProductAttributes::findOne($inventoryDetail->attribute_id)->name?></strong></td>
                      <td width=50%><?=$inventoryDetail->attribute_value?></td>
                    </tr>
                  </tbody>
                </table>
			  <?php
			  }
			  ?>
              </div>
              <div id="tab-review" class="tab-pane">

                  <div id="review">
                    <div>
                      <table class="table table-striped table-bordered">
                        <tbody>
						<?php
						foreach($product_reviews as $review)
						{
							$user = User::find()->where("entity_type='customer' and entity_id=".$review->customer_id)->one();
						?>
                          <tr>
						    <td style="width: 20%;"><input type="text" class="multe-rating-nocap-sm" value="<?=$review->rating?>" readonly></td>
                            <td ><strong><span><?=$user->first_name?> <?=$user->last_name?></span></strong></td>
                            <td class="text-right"><span><?=date('M d, Y', $review->added_at)?></span></td>
                          </tr>
                          <tr>
                            <td colspan="3">
							  <p style="white-space: pre-wrap;"><?=$review->review?></p>
                            </td>
                          </tr>
						<?php
						}
						?>
                        </tbody>
                      </table>
                     
                    </div>
                    <div class="text-right"></div>
                  </div>

              </div>
            </div>
          </div>
         </div>
      </div>
</div>
            <h3 class="subtitle"><?=Yii::t('app', 'Related Products')?></h3>
            <div class="owl-carousel related_pro">
			<?php
			$itemsList = Inventory::find()
								->alias('i')
								->joinWith('inventoryProducts p')
								->where('p.category_id = '.Product::findOne($inventory->product_id)->category_id)
								->andWhere('p.sub_category_id = '.Product::findOne($inventory->product_id)->sub_category_id)
								->andWhere("i.id !=".$inventory->id)
								->orderBy('name')
								->limit(15)
								->asArray()
								->all();
			foreach($itemsList as $item)
			{
				$fileDet = File::find()->where("entity_type='product' and entity_id=".$item['product_id'])->one();

				$inventoryPrice = floatval($item['price']);
				if($item['price_type'] == 'B')
				{
				  foreach(json_decode($item['attribute_price']) as $row)
				  {
					  $inventoryPrice += floatval($row);
				  }
				}

				if($item['discount_type'] == 'P')
				$inventoryDiscount = $item['discount'];
				else
				{
					if($inventoryPrice > 0)
						$inventoryDiscount = round((floatval($item['discount'])/$inventoryPrice)*100,2);
					else
						$inventoryDiscount = 0;
				}

				$inventoryDiscountedPrice = round($inventoryPrice - $inventoryPrice*$inventoryDiscount/100, 2);
			?>
              <div class="product-thumb">
			  <?php
			    $url1 = Url::to(['/product/default/detail', 'inventory_id' => $item['id']]);
			  ?>
                <div class="image"><a href="<?=$url1?>"><img src="<?=Url::base()?>/../../multeback/web/attachments/<?=$fileDet->id?><?=strrchr($fileDet->file_name, ".")?>" alt="<?=$item['product_name']?>" title="<?=$item['product_name']?>" class="img-responsive img-listing" /></a></div>
                <div class="caption">
                  <h4><a href="<?=$url1?>"><?=$item['product_name']?></a></h4>
                  <p class="price"> 
					  <span class="price-new"><?=MulteModel::formatAmount($inventoryDiscountedPrice)?></span> 
					  <?php
					  if ($inventoryPrice != $inventoryDiscountedPrice)
					  {
					  ?>
					  <span class="price-old"><?=MulteModel::formatAmount($inventoryPrice)?></span> 
					  <span class="saving">-<?=$inventoryDiscount?>%</span> 
					  <?php
					  }
					  ?>
				  </p>
                  <p><input type="text" class="multe-rating-nocap-sm" value="<?=$item['product_rating']?>" readonly> </p>
                </div>
                <div class="button-group">
				<?php
				if($item['stock'] > 0)
				{
				?>
                    <a href="<?=$url1?>"><button class="btn-primary " type="button" value="<?=$item['id']?>"><span><?=Yii::t('app', 'Buy Now')?></span></button></a>
<!--                  <button class="btn-primary addtocart" type="button" value="<?/*=$item['id']*/?>"><span><?/*=Yii::t('app', 'Buy Now')*/?></span></button>
-->				<?php
				}
				else
				{
				?>
				  <button class="btn-primary" type="button" onClick="" disabled><span><?=Yii::t('app', 'Out of Stock')?></span></button>
				<?php
				}
				?>

                </div>
              </div>
			<?php
			}
			?>
            </div>
          </div>
        </div>
        <!--Middle Part End -->
		<!--Right Part Start -->
		<!--<aside id="column-right" class="col-sm-3 hidden-xs">
		  <h3 class="subtitle"><?/*=Yii::t('app', 'Other offerings')*/?></h3>
		  <div class="side-item">-->
		  <?php
		  $itemsList = Inventory::find()
								->where("product_id=".$inventory->product_id)
								->andwhere("attribute_values='".$inventory->attribute_values."'")
								->andWhere("id !=".$inventory->id)
								->limit(6)
								->all();
		  foreach($itemsList as $item)
		  {
			  $fileDet = File::find()->where("entity_type='product' and entity_id=".$item['product_id'])->one();

				$inventoryPrice = floatval($item['price']);
				if($item['price_type'] == 'B')
				{
				  foreach(json_decode($item['attribute_price']) as $row)
				  {
					  $inventoryPrice += floatval($row);
				  }
				}

				if($item['discount_type'] == 'P')
				$inventoryDiscount = $item['discount'];
				else
			    {
					if($inventoryPrice > 0)
						$inventoryDiscount = round((floatval($item['discount'])/$inventoryPrice)*100,2);
					else
						$inventoryDiscount = 0;
				}

				$inventoryDiscountedPrice = round($inventoryPrice - $inventoryPrice*$inventoryDiscount/100, 2);
		  ?>
			<div class="product-thumb clearfix">
			<?php
			  $url1 = Url::to(['/product/default/detail', 'inventory_id' => $item['id']]);
			?>
			  <div class="image"><a href="<?=$url1?>"><img src="<?=Url::base()?>/../../multeback/web/attachments/<?=$fileDet->id?><?=strrchr($fileDet->file_name, ".")?>" alt="<?=$item->product_name?>" title="<?=$item->product_name?>" class="img-responsive" /></a></div>
			  <div class="caption">
				<h4><a href="<?=$url1?>"><?=$item->product_name?></a></h4>
				<strong><?=Yii::t('app', 'Sold By')?>:</strong> 
				<?= Html::a(Vendor::findOne($item['vendor_id'])->vendor_name, Url::to(['/product/default/filter']), [
																'data'=>[
																	'method' => 'post',
																	'params'=>['vendor_id'=> $item['vendor_id']],
																],
																'data-toggle' => 'tooltip',
																'title' => Yii::t('app', 'Click To View All Products By This Seller'),
															]) ?>
						
				<div class="row">
					<div class="col-sm-6">
						<strong><?=Yii::t('app', 'Seller Rating')?>:</strong>
					</div>
					<div class="col-sm-6">
						<input type="text" class="multe-rating-nocap-sm" value="<?=$item->vendor_rating?>" readonly>
					</div>
				</div>
				<p class="price">
					<span class="price-new"><?=MulteModel::formatAmount($inventoryDiscountedPrice)?></span> 
					<?php
					if ($inventoryPrice != $inventoryDiscountedPrice)
					{
					?>
					<span class="price-old"><?=MulteModel::formatAmount($inventoryPrice)?></span> 
					<span class="saving">-<?=$inventoryDiscount?>%</span>
					<?php
					}
					?>
				</p>
			  </div>
			</div>
		  <?php
		  }
		  ?>
		  </div>
		</aside>
		<!--Right Part End -->
	</div>
  
</div>
</div>
<div class="modal fade confirmmodal" >
  <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-body">
          <h4><p class="text-center"><?=Yii::t('app', 'Added')?>! <i class="glyphicon glyphicon-ok text-success"></i></p></h4>
        </div>
      </div>
  </div>
</div>


<script>
    function getbanks(paymethd){
        $("#banks").empty();
        var id = paymethd.options[paymethd.selectedIndex].value;
        if(id ==1) {

        $("#banks").append('<label class="control-label" for="input-option-lbl-advance">Select a Bank</label>'+
            '<select class="bank form-control rounded"  name="bank" id="bank" onChange="getplan(this)" required>'+
            '<option value="">Select.................</option>'+
            '</select>');


        var url = '<?=Yii::$app->homeUrl?>ijax/getbank';
        //alert(url);
        $.ajax({
            type: 'GET',
            url: url,
            data:{
                id:id,
            },
            dataType:'json',
            success:function(res){
                for (var i = 0; i < res.count; i++)
                {
                    var id= 'res.id'+i;
                    var bank = 'res.bank'+i;
                    $('.bank').append('<option value="'+eval(id)+'">'+eval(bank)+'</option>');
                }
            }
        })

        }
    }

    function getplan(bank) {
        var bank_id = bank.options[bank.selectedIndex].value;
        //alert(bank_id);
        $("#plans").empty();
        $("#plans").append('<label class="control-label" for="input-option-installment-plan">Installment Plan</label>'+
            '<select class="plan form-control rounded"  name="plan_id" id="plan" onChange="SelectChanged(this)" required>'+
            '<option value="">Select.................</option>'+
            '</select>');
        var bank_id = bank.options[bank.selectedIndex].value;
        var url = '<?=Yii::$app->homeUrl?>ijax/get-plans';
        $.ajax({
            type:'GET',
            url: url,
            data:{
                bank_id:bank_id,
            },
            dataType:'json',
            success:function(res){

                for (var i = 0; i < res.count; i++)
                {
                    var id= 'res.id'+i;
                    var plan = 'res.plan'+i;
                    $('.plan').append('<option value="'+eval(id)+'">'+eval(plan)+'</option>');
                }
            }
        });

    }
    function SelectChanged(item){
        var price = '<?=$inventory->product_id?>';

        $("#showCalculator").empty();
        var id = item.options[item.selectedIndex].value;
        var url = '<?=Yii::$app->homeUrl?>ijax/get-installments';
        $.ajax({
            type:'GET',
            url: url,
            data:{
                id:id,
                price:price,
            },
            dataType:'json',
            success:function(res){
                $("#showCalculator").show();
                $("#showCalculator").append('<span style="color: #ed5565;"> Advance Rs:&nbsp;'+res.installment0+'</span>&emsp;EMI Rs:&nbsp;&nbsp;'+res.installment1+'&emsp;Total Payable Rs:&nbsp;&nbsp;'+res.total+'<input type="hidden" name="payable" value="'+res.total+'"');

            }
        });
    }

</script>