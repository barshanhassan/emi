<?php
use multebox\models\File;
use multebox\models\Product;
use multebox\models\ProductCategory;
use multebox\models\ProductSubCategory;
use multebox\models\ProductSubSubCategory;
use multebox\models\ProductBrand;
use multebox\models\ProductAttributes;
use multebox\models\ProductAttributeValues;
use multebox\models\InventoryDetails;
use multebox\models\Inventory;
use multebox\models\Vendor;
use multebox\models\search\MulteModel;
use multebox\models\ProductReview;
use multebox\models\User;
use yii\helpers\Url;
use yii\helpers\Html;

include_once('../web/cart_script.php');
$lang = $_REQUEST['lang'];
$product = Product::findOne($inventory->product_id);
$fileDetails = File::find()->where("entity_type='product' and entity_id='$inventory->product_id'")->orderBy("id asc")->all();
$product_reviews = ProductReview::find()->where("product_id=".$inventory->product_id)->all();
?>







    <style id="compiled-css" type="text/css">
        .carousel-item {
            height: 100vh;
            min-height: 350px;
            background: no-repeat center center scroll;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }

        .for-bg-img::after {
            display: block;
            content: "";
            background-image: url(<?= Yii::$app->homeUrl?>/img/homepage-path-2.png);
            width: 50%;
            height: 100%;
            background-size: cover;
            right: 0px;
            position: absolute;
        }

        .bef::before {
            content: "";
            background-image: url(<?= Yii::$app->homeUrl?>/img/homepage-bitmap-7.png);
        }

        /* .for-bg-img::before {
                    display: block;
                    content: "";
                    background-image: url(./img/homepage-bitmap.png);
                    width: 50%;
                    height: 60%;
                    background-size: cover;
                    right: 230px;
                    position: absolute;
                    z-index: 99;
                    top: 330px;
                } */
        .col-2-custom-productpage {
            width: 80% !important;
            flex: 0 0 40.566667%;
            max-width: 40.666667%;
            height: 80px !important;
        }

        .nav-tabs .nav-item.show .nav-link,
        .nav-tabs .nav-link.active {
            color: #ffffff;
            background-color: #f7b04f;
            border-color: #dee2e6 #dee2e6 #fff;
        }

        .tab-content>.active {
            padding: 0;
        }

        .productname-moretiles {
            font-family: "Helvetica-Light", Helvetica, Arial, serif;
            font-size: 19.0px;
            color: rgba(0, 0, 0, 1.0);
            text-align: left;
            letter-spacing: 0.86px;
            line-height: 23.0px;
        }
        button:disabled {
            cursor: not-allowed;
            pointer-events: all !important;
        }
        .size:lang(ur){
           font-size: 20px;
        }

        .checkbox {
            margin-top: 0.5rem;
            margin-bottom: 1rem;
        }

    </style>



<script>
    $(document).ready(function()
    {
        $('.addtocart2').on('click', function () {
            $("#cerror").empty();
            if($('#color').val() == 0) {

                $("#cerror").show();
                $("#cerror").append('<span class="font" style="color: #ed5565;"><?=Yii::t("app","Please Select Color First")?>');

                return false;

            }
            else if($('#input-quantity').val() == 0){


                $("#cerror").show();
                $("#cerror").append('<span  class="font" style="color: #ed5565;"><?=Yii::t("app","Please Select Quantity First")?>');
                return false;
            }


            var pay_method  = $('#payment-method').val();
            if(pay_method == 1 ){
                var bank_id  = $('#banks').val();
                var plan_id  = $('#plan').val();

            }else{
                var bank_id  = 0;
                var plan_id  = 0;
            }



            var cart = $('.mycart');
            var imgtodrag = $('.image').find("img").eq(0);
            if (imgtodrag) {
                var imgclone = imgtodrag.clone()
                    .offset({
                        top: imgtodrag.offset().top,
                        left: imgtodrag.offset().left
                    })
                    .css({
                        'opacity': '0.5',
                        'position': 'absolute',
                        'height': '150px',
                        'width': '150px',
                        'z-index': '100'
                    })
                    .appendTo($('body'))
                    .animate({
                        'top': cart.offset().top + 10,
                        'left': cart.offset().left + 10,
                        'width': 75,
                        'height': 75
                    }, 1000, 'easeInOutExpo');

                imgclone.animate({
                    'width': 0,
                    'height': 0
                }, function () {
                    $(this).detach()
                });
            }

            $.post("<?=Url::to(['/order/default/ajax-add-to-cart'])?>", { 'inventory_id': $(this).val(), 'total_items' : $('#input-quantity').val(),'payment_method' : pay_method, 'bank_id' : bank_id, 'plan_id' : plan_id,'_csrf' : '<?=Yii::$app->request->csrfToken?>'}).done(function(result){


                if(result == 'alreadyerror'){
                    $('.errormodal').modal('show');
                    setTimeout(function() {$('.errormodal').modal('hide');}, 1500);
                }else{

                    $('.carttable tbody').html(result);

                    $('.cartcount').html($('.hiddencartvalue').val());
                    $('.remainingstock').val($('.hiddenremainingstock').val());
                    $('.confirmmodal').modal('show');
                    setTimeout(function() {$('.confirmmodal').modal('hide');}, 1500);
                    var x= $('.hiddencartvalue').value();
                    alert(x);
                }

            }).fail(function() {
                alert('error');
                /*$('.errormodal').modal('show');
                setTimeout(function() {$('.errormodal').modal('hide');}, 1500);*/


            });

            //$.get("<?=Url::to(['/product/default/detail'])?>", { 'inventory_id': $(this).val()}) .done(function(data){ $( "body" ).html(data);});

        });

        //var error='';
        $(".plus").click(function(event)
        {
            //var stock = <?=$inventory->stock?>;
            var stock = $('.remainingstock').val();
            //Remove_Error($(this));
            if(stock < parseInt($('#input-quantity').val()))
            {
                //alert($('#input-quantity').val());
                //error+=Add_Error($(this),'<?=Yii::t('app','Not enough stock!')?>');
                $('#input-quantity').val(stock);
                //event.preventDefault();
                return false;
            }
        });

        $('#input-quantity').change(function(event)
        {
            //var stock = <?=$inventory->stock?>;
            var stock = $('.remainingstock').val();
            //Remove_Error($(this));
            if(stock < parseInt($('#input-quantity').val()))
            {
                //alert("Not enough stock!");
                //error+=Add_Error($(this),'<?=Yii::t('app','Not enough stock!')?>');
                $('#input-quantity').val(stock);
                //event.preventDefault();
                return false;
            }
        });

        //Remove_Error($(this));

        // Elevate Zoom for Product Page image
        $("#zoom_01").elevateZoom({
            gallery:'gallery_02',
            cursor: 'pointer',
            galleryActiveClass: 'active',
            imageCrossfade: true,
            zoomWindowFadeIn: 500,
            zoomWindowFadeOut: 500,
            lensFadeIn: 500,
            lensFadeOut: 500
        });
//////pass the images to swipebox
        $('.source-img').on('click', function(){
           var src = $(this).attr('src');
            $('#zoom_01').removeAttr('data-zoom-image');
            $('#zoom_01').attr('data-zoom-image', src);
            $('#zoom_01').removeAttr('src');
            $('#zoom_01').attr('src', src);
            $(".zoomWindow").attr("style", "background-image: url("+src+")");
            $(".zoomWindow").css({ "width": "400px","height": "400px","background-size":"1000px 1000px","overflow":"hidden","text-align":"center","z-index":"100","background-color":"rgb(255, 255, 255)","background-repeat":"no-repeat","position":"absolute","top":"0px","left":"450px","float":"left","dispaly":"none"});
            // $(".zoomWindowContainer").css("background-image", src);
        });



    });
</script>
<input type="hidden" class="remainingstock" value="<?=$inventory->stock?>">

<div class="loan-form">
    <section class="mt-6 py-5">
        <div class="container">
            <div class="border-0 card">
                <div class="row">
                    <aside class="col-sm-7">
                        <article class="gallery-wrap">
                            <div class="img-big-wrap m-3">
                                <div class="image">
                                    <img class="" itemprop="image" id="zoom_01"  src="<?=Url::base()?>/../../multeback/web/attachments/<?=$fileDetails[0]->id?><?=strrchr($fileDetails[0]->file_name, ".")?>" title="<?=$fileDetails[0]->file_title?>" alt="<?=$fileDetails[0]->file_title?>" data-zoom-image="<?=Url::base()?>/../../multeback/web/attachments/<?=$fileDetails[0]->id?><?=strrchr($fileDetails[0]->file_name, ".")?>"/>
                                </div>
                            </div>

                            <!-- slider-product.// -->
                            <div class="img-small-wrap mt-3">
                                <?php
                                foreach($fileDetails as $file)
                                {
                                    ?>
                                    <div class="item-gallery"> <img itemprop="image"  src="<?=Url::base()?>/../../multeback/web/attachments/<?=$file->id?><?=strrchr($file->file_name, ".")?>" title="<?=$file->file_title?>" alt = "<?=$file->file_title?>" class="source-img img-thumbnail-list" /></div>
                                    <?php
                                }
                                ?>

                            </div>
                            <!-- slider-nav.// -->
                        </article>
                        <!-- gallery-wrap .end// -->
                    </aside>
                    <aside class="col-sm-5">
                        <article class="card-body">
                            <h3 class="title mb-3 productname"><?=$inventory->product_name?></h3>
                            <p class="price-detail-wrap">
                                    <span class="price h3 product-cost">
                                        <span class="currency"></span><span class="num">Rs<?=MulteModel::formatAmount($inventory->price)?></span>
                                    </span>

                                &nbsp;&nbsp;&nbsp&nbsp<span>Status: </span><span class="text-success font"><strong><?=Yii::t('app','In Stock')?></strong></span>

                            </p>
                            <!-- price-detail-wrap .// -->
                            <dl class="param param-inline quantity">
                                <dd>
                                    <select class="form-control pr-5 font" id="color">
                                        <option value="0"><?=Yii::t('app','Select Color')?></option>
                                        <?php
                                        $obj = json_decode($inventory->attribute_values);
                                        if(empty($obj)){?>
                                            <option value="black"> <?= Yii::t('app','Black')?> </option>
                                        <?php }
                                        foreach ($obj as $attri){?>
                                                <option value="<?=$attri?>"> <?= Yii::t('app',$attri)?> </option>
                                        <?php

                                        }
                                        ?>

                                    </select>
                                </dd>
                            </dl>

                            <dl class="param param-inline quantity ml-3">
                                <dd>
                                    <select class="form-control pr-5 font" id="input-quantity">
                                        <option value="0"><?=Yii::t('app','Quantity')?></option>
                                        <option selected value="1"> 1 </option>
                                    </select>
                                </dd>
                            </dl>

                            <dl class="param param-inline  required">
                                <dd>
                                <select name="payment_method" id="payment-method" class="form-control font size" onchange="getbanks(this)" required>
                                    <option value="2" ><?=Yii::t('app','Full Cash Payment')?></option>
                                    <?php if($inventory->price > 5000){ ?>
                                    <option value="1" selected="selected" ><?=Yii::t('app','Installments and Insurance')?></option>
                                    <?php } ?>
                                </select>
                                </dd>
                            </dl>


                           <!-- <div class="checkbox">

                                <label>
                                    <input type="checkbox"  id="spayment" value="30" ><i class="helper"></i>Rs. 30/ 1 Month Kist Pay Security Fee <img class="log43" style="" width="35px" height="35px" src="<?/*= Yii::$app->homeUrl*/?>img/KistPaylogo.png"></label>

                            </div>-->
                            <?php if($inventory->price > 5000){ ?>

                            <div id="installment">


                            <p class="font"><?= Yii::t('app','Please Choose one Bank:')?></p>
                            <dl class="param param-feature">
                                <dt>
                                    <div class="row" id="banks">
                                        <div class="col-2 col-2-custom col-2-custom-productpage ml-3"><img class="bitmap1" id="1" onclick="javascript:getplan(1)" src="<?= Yii::$app->homeUrl?>img/homepage-bitmap-1@2x.png"></div>
                                    </div>
                                </dt>
                            </dl>

                            <dl class="param param-inline payment-plan">
                                <dd>
                                    <select class="form-control pr-5 font" id="plan" onChange="SelectChanged(this)">
                                        <option><?=Yii::t('app','Select Installment Plan')?></option>

                                    </select>
                                </dd>
                            </dl>

                            <br>
                            <dl class="param param-inline payment-plan">
                                <dd>
                                    <div id="showCalculator" class="row font" style="margin-bottom:7px;"></div>
                                </dd>
                            </dl>
                            </div>
                            <?php } ?>
                            <div id="cerror" class="row" style="margin-left:7px; color: red"></div>
                            <br>
                            <div class="buttons">
                                <button id="button-cart"  class="font add-to-cart btn btn-lg btn-primary mr-2 addtocart2 " <?= ($inventory->id != 28) ? 'disabled="disabled"': '' ?> title="Please Select Installement Plan" type="button" value="<?=$inventory->id?>"><?=Yii::t('app','Add to Cart')?> </button>

                                <a href="<?=Yii::$app->homeUrl?><?php echo ($lang == 'ur-UR' ? 'order/default/cart?lang=ur-UR':'order/default/cart')?>" class="add-to-cart btn btn-lg btn-primary font"><?=Yii::t('app','CHECKOUT')?>  </a>
                            </div>
                            <!-- Nav tabs -->
                            <ul class="mt-5 nav nav-tabs">
                                <li class="nav-item">
                                    <a class="nav-link pl-5 pr-5 active font" data-toggle="tab" href="#home"><?=Yii::t('app','SPECIFICATIONS')?></a>
                                </li>
                               <!-- <li class="nav-item">
                                    <a class="nav-link pl-5 pr-5" data-toggle="tab" href="#menu1">REVIEWS</a>
                                </li>-->
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane container active" id="home">
                                    <?=$product->description?>
                                </div>
                                <?php
                                foreach($product_reviews as $review)
                                {
                                $user = User::find()->where("entity_type='customer' and entity_id=".$review->customer_id)->one();
                                ?>
                                <div class="tab-pane container fade" id="menu1"><?=$review->review?></div>
                                <?php } ?>
                            </div>
                        </article>
                        <!-- card-body.// -->
                    </aside>
                    <!-- col.// -->
                </div>
                <!-- row.// -->
            </div>
        </div>
    </section>
    <?php $inventoryItemsList = Inventory::find()->where('stock > 0')->orderBy(['id'=>SORT_DESC])->limit(15)->all(); ?>
<section class="mt-6 py-5">
        <div class="container">
            <h2 class="mb-5 moreexclusiveoffer"><?=Yii::t('app','More Exclusive Offers')?></h2>
            <div class="block">
                <div class="row">
                    <?php $cnt = 0;
                    foreach($inventoryItemsList as $inventoryItem)
                    {   $cnt++;
                    $fileDetails = File::find()->where("entity_type='product' and entity_id=$inventoryItem->product_id")->one();
                    $price = Inventory::find()->where(['=','product_id',$inventoryItem->product_id])->one()->price;
                        if($lang=='ur-UR'){
                            $url=Url::to(['/product/default/prodetail', 'inventory_id' => $inventoryItem->id,'lang'=>'ur-UR']);
                        }else{
                            $url=Url::to(['/product/default/prodetail', 'inventory_id' => $inventoryItem->id]);
                        }
                    ?>

                    <div class="col-4">
                        <div class="mr-2 span4">
                            <div class="row">
                                <div class="col-5 mb-4 mt-4">
                                    <div class="content-heading">
                                        <a href="<?=$url?>"><h3 class="productname-moretiles"><?=$inventoryItem->product_name?></h3></a>
                                    </div>
                                    <p class="font"><?=Yii::t('app','Full Price')?>: Rs<?=MulteModel::formatAmount($price)?></p>
                                    <?php if($price > 5000){   ?>
                                        <p class="card-text m-0 font"><?=Yii::t('app','EMI as low as')?>: Rs <?=MulteModel::formatAmount(MulteModel::getEmi($price))?></p>

                                    <?php } ?>
                                    <a href="<?=$url?>" class="btn btn-lg btn-primary details-btn mr-2 font"><?=Yii::t('app','DETAILS')?></a>
                                </div>
                                <div class="col-7"><a href="<?=$url?>"><img class="img-left img-more-offers" src="<?=Url::base()?>/../../multeback/web/attachments/<?=$fileDetails->id?><?=strrchr($fileDetails->file_name, ".")?>" style="width:100%;"></a></div>
                            </div>
                        </div>
                    </div>
                        <?php if($cnt == 3){
                            break;
                        }
                    } ?>

                </div>
                <br>
                <div class="row">
                    <?php $cnt = 0;
                    foreach(array_reverse($inventoryItemsList) as $inventoryItem)
                    {   $cnt++;
                        $fileDetails = File::find()->where("entity_type='product' and entity_id=$inventoryItem->product_id")->one();
                        $price = Inventory::find()->where(['=','product_id',$inventoryItem->product_id])->one()->price;
                        if($lang=='ur-UR'){
                            $url=Url::to(['/product/default/prodetail', 'inventory_id' => $inventoryItem->id,'lang'=>'ur-UR']);
                        }else{
                            $url=Url::to(['/product/default/prodetail', 'inventory_id' => $inventoryItem->id]);
                        }
                        ?>

                        <div class="col-4">
                            <div class="mr-2 span4">
                                <div class="row">
                                    <div class="col-5 mb-4 mt-4">
                                        <div class="content-heading">
                                            <a href="<?=$url?>"><h3 class="productname-moretiles"><?=$inventoryItem->product_name?></h3></a>
                                        </div>
                                        <p class="font"><?=Yii::t('app','Full Price')?>: Rs<?=MulteModel::formatAmount($price)?></p>
                                        <?php if($price > 5000){   ?>
                                            <p class="card-text m-0 font"><?=Yii::t('app','EMI as low as')?>: Rs <?=MulteModel::formatAmount(MulteModel::getEmi($price))?></p>

                                        <?php } ?>
                                        <a href="<?=$url?>" class="btn btn-lg btn-primary details-btn mr-2 font"><?=Yii::t('app','DETAILS')?></a>
                                    </div>
                                    <div class="col-7"><a href="<?=$url?>"><img class="img-left img-more-offers" src="<?=Url::base()?>/../../multeback/web/attachments/<?=$fileDetails->id?><?=strrchr($fileDetails->file_name, ".")?>" style="width:100%;"></a></div>
                                </div>
                            </div>
                        </div>
                        <?php if($cnt == 3){
                        break;
                    }
                    } ?>


                </div>
            </div>
            <div class="col-5" style="margin:50px auto;">
                <a href="<?=Yii::$app->homeUrl?>product/default/index"><button type="button" class="btn btn-circle-cus btn-lg btn-primary font"><?=Yii::t('app','VIEW MORE MOBILES')?></button></a>
            </div>
        </div>
    </section>
</div>

<script>
    function getbanks(paymethd){

        var id = paymethd.options[paymethd.selectedIndex].value;

        if(id == 2){
            $('#installment').hide();
            $('#button-cart').removeAttr('disabled','disabled');
            $('#button-cart').removeAttr('title','Please Select Installment Plan');

        }
        else
        {
            $('#installment').show();
            $('#button-cart').attr('disabled','disabled');
            $('#button-cart').attr('title','<?=Yii::t("app","Please Select Installment Plan")?>');
        }

        /*if(id ==1) {

            $("#banks").append('<label class="control-label" for="input-option-lbl-advance">Select a Bank</label>'+
                '<select class="bank form-control rounded"  name="bank" id="bank" onChange="getplan(this)" required>'+
                '<option value="">Select.................</option>'+
                '</select>');


            var url = 'url/ijax/getbank';
            //alert(url);
            $.ajax({
                type: 'GET',
                url: url,
                data:{
                    id:id,
                },
                dataType:'json',
                success:function(res){
                    for (var i = 0; i < res.count; i++)
                    {
                        var id= 'res.id'+i;
                        var bank = 'res.bank'+i;
                        $('.bank').append('<option value="'+eval(id)+'">'+eval(bank)+'</option>');
                    }
                }
            })

        }*/
    }

    function getplan(bank_id) {
       // var bank_id = bank.options[bank.selectedIndex].value;
        //alert(bank_id);
        $("#banks").prop("value",bank_id);


        $('.a01').css('opacity', '0.2');
        $('.a01').attr('onclick', 'javascript:getplan(1)');
        $('#'+bank_id).css('opacity', '1');
        $('#'+bank_id).removeAttr('onclick', 'javascript:getplan(1)');
        $("#plan").empty();


        var url = '<?=Yii::$app->homeUrl?>ijax/get-plans';
        $.ajax({
            type:'GET',
            url: url,
            data:{
                bank_id:bank_id,
            },
            dataType:'json',
            success:function(res){
                $('#plan').append('<option value="'+eval(id)+'"><?=Yii::t('app','Select Installment Plan')?></option>');
                for (var i = 0; i < res.count; i++)
                {
                    var id= 'res.id'+i;
                    var plan = 'res.plan'+i;

                    $('#plan').append('<option value="'+eval(id)+'">'+eval(plan)+'</option>');
                }
            },
            error: function(data){
                console.log(data + "ERROR")
                alert("Something wrong, try again!")
                location.reload();
            }
        });

    }
    function SelectChanged(item){
        var price = '<?=$inventory->product_id?>';

        $("#showCalculator").empty();
        $('#button-cart').attr('disabled','disabled');
        var id = item.options[item.selectedIndex].value;
        var url = '<?=Yii::$app->homeUrl?>ijax/get-installments';
        $.ajax({
            type:'GET',
            url: url,
            data:{
                id:id,
                price:price,
            },
            dataType:'json',
            success:function(res){
                $("#showCalculator").show();


                $("#showCalculator").append('<span style="color: #ed5565;"><?=Yii::t('app','Advance Rs')?>:&nbsp;'+parseInt(res.installment0)+'</span>&emsp;<?=Yii::t('app','EMI Rs')?>:&nbsp;&nbsp;'+parseInt(res.installment1)+'&emsp;<?=Yii::t('app','Total Payable Rs')?>:&nbsp;&nbsp;'+parseInt(res.total)+'<input type="hidden" name="payable" value="'+res.total+'"');
                $('#button-cart').removeAttr('disabled','disabled');
                $('#button-cart').removeAttr('title','Add to Cart');
            }
        });
    }

</script>



