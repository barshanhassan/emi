<?php

namespace multefront\modules\payments;

/**
 * paymentsplans module definition class
 */
class PaymentPlanTemplate extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'multefront\modules\payments\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
