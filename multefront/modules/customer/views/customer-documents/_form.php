<?php

use yii\helpers\Html;
use kartik\file\FileInput;
//use yii\helpers\Html;
use yii\bootstrap\ActiveForm ;
use multebox\models\DocumentTypes;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerDocuments */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-documents-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

    <?php  $form->field($model, 'customer_id')->textInput();

    /*$user = multebox\models\User::findOne(Yii::$app->user->identity->id);

    $id = $_REQUEST['id']; */
    ?>


    <?php
        $query = DocumentTypes::find()->all();
        $count = 1;
        foreach ($query as $val){?>
            <div class="row">
                <div class="col-lg-2" style="padding-top: 20px">
                    <label for="logo" ><?=$val->name ?></label>
                </div>
                <input type="hidden" name="document_type[]" value="<?=$val->name?>">
                <input type="hidden" name="customer_id" value="<?=Yii::$app->user->identity->id?>">
                <div class="col-lg-6" style="padding-top: 20px">



                    <?php  echo FileInput::widget([
                        'options'=>[
                            'multiple'=>true
                        ],
                        'name' => 'docs[]',
                        'pluginOptions' => [
                            'allowedFileExtensions'=>['jpg', 'jpeg', 'pdf','png'],
                            'showPreview' => true,
                            'showCaption' => false,
                            'showRemove' => true,
                            'showUpload' => true,
                        ]
                    ]); ?>
                </div>
            </div>


        <?php $count++; }

    ?>


    <?php $form->field($model, 'file_name')->textInput(['maxlength' => true]) ?>

    <?php $form->field($model, 'file_size')->textInput(['maxlength' => true]) ?>

    <?php $form->field($model, 'file_type')->textInput(['maxlength' => true]) ?>

    <?php $form->field($model, 'document_type')->textInput(['maxlength' => true]) ?>

    <?php $form->field($model, 'status')->textInput() ?>



    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
