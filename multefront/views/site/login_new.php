<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \multebox\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

?>

<style>

    .card {

        color: red;
    }
    .card-signup {
        width: 42%;
        margin: 0 auto;
        position: relative;
        left: 0;
        margin-top: 18%;
        right: 0;
        bottom: -38%;
    }

    .field-icon {
        float: right;
        margin-left: -25px;
        margin-top: -25px;
        position: relative;
        z-index: 2;
        color: #000;
    }

</style>
<?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

    <div class="loan-form signup-form">
        <section class="py-5 sec-loanform signup-page">
            <div class="container">
                <div class="row">
                    <div class="col-4 float-left text-top">
                        <h2 class="whetheryouhaveac"><?=Yii::t('app','Smart Loan')?><br><?=Yii::t('app','for')?>&nbsp;<?=Yii::t('app','Smart Phones')?>
                        </h2>
                    </div>
                    <div class="col-6 float-right img-top-signup">
                        <img src="<?=Yii::$app->homeUrl?>img/sign-up-group-421.png">
                    </div>
                </div>
                <div class="box-shad-light card card-sign card-signup">
                    <div class="card-body">


                        <div class="col-12 ">

                            <?= $form->field($model, 'username', [
                                'template' => '{input}{beginLabel}{labelTitle}{endLabel}<i class="bar"></i>{error}{hint}',
                                'labelOptions' => ['class' => 'control-label font']
                            ])->textInput(['id' => 'email', 'class' => 'font']); ?>
                        </div>

                        <div class="col-12">
                            <?= $form->field($model, 'password', [
                                'template' => '{input}{beginLabel}{labelTitle}{endLabel}<i class="bar"></i> <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>{error}{hint}',
                                'labelOptions' => ['class' => 'control-label font']
                            ])->passwordInput(['class' => 'font','id'=>'password-field']); ?>
                        </div>


                        <div class="col-12">

                            <a href="<?=Yii::$app->homeUrl?><?php echo ($_REQUEST['lang'] == 'ur-UR' ? 'site/request-password-reset?lang=ur-UR' : 'site/request-password-reset')?>"><p class="font" style="text-align: right; "><?php echo Yii::t('app','Forgot password?')?></p></a>

                        </div>


                        <div class="col-12">
                            <div class="button-container">
                                <?= Html::submitButton(Yii::t('app', '<span>Login</span>'), ['class' => 'button m-0 font', 'name' => 'login-button']) ?>
                                <a href="<?=Yii::$app->homeUrl?><?php echo ($_REQUEST['lang'] == 'ur-UR' ? 'site/signup?lang=ur-UR' : 'site/signup')?>"><button class="button m-0 font" type="button" value="Register"><span><?=Yii::t('app', '<span>Register</span>')?></span></button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php ActiveForm::end(); ?>

<script>
    $(".toggle-password").click(function() {

        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });

    $("input").prop('required',true);

</script>