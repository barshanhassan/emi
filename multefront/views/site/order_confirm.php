
<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('app', 'Order Confirm');
$this->params['breadcrumbs'][] = $this->title;
?>

<style>

    .card-signup {
        width: 42%;
        margin: 0 auto;
        position: relative;
        left: 0;
        margin-top: 15%;
        right: 0;
        bottom: -38%;
    }
</style>

<div class="loan-form signup-form">
    <section class="py-5 sec-loanform ">
        <div class="container">



            <div class="box-shad-light card card-sign card-signup">

                <div class="card-body font">
                    <?php
                        echo Yii::t('app', 'Thank you for placing Your order. Your Order #00000 and will be delivered to you in the next working 4 working days.');

                    ?>
                </div>
            </div>
        </div>
    </section>
</div>

<script>

    $("input").prop('required',true);
</script>

