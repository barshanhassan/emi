<style>
    .buttons {
        position: relative;
        width: 96%;
    }

    .table {
        width: 92%;
        margin-bottom: 1rem;
        color: #212529;
        margin: 0 auto !important;
    }

</style>

<?php
use multebox\models\Order;
use multebox\models\OrderStatus;
use multebox\models\City;
use multebox\models\State;
use multebox\models\Country;
use multebox\models\SubOrder;
use multebox\models\PaymentMethods;
use multebox\models\search\MulteModel;
use multebox\models\Inventory;
use multebox\models\DigitalRecords;
use multebox\models\File;
use multebox\models\Vendor;
use multebox\models\Cart;
use multebox\models\LicenseKeyCode;
use yii\helpers\Url;
use yii\helpers\Json;

?>

<script>
$(document).on("click", '#cancelitem', function(event)
{
	if (confirm('Are you sure You want to cancel this item from the order?'))
	{
		$('.tooltip-inner').remove();
		$('.tooltip-arrow').remove();

		var sub_order_id = $(this).val();
		$.post("<?=Url::to(['/order/default/cancel-sub-order'])?>", { 'sub_order_id': sub_order_id, '_csrf' : '<?=Yii::$app->request->csrfToken?>'}) .done(function(result){
					//alert(result);
					$('.orderitems tbody').html(result);
				})

		$('body').tooltip({
			selector: '[data-toggle="tooltip"]'
		});
	}
});
</script>

<section class="mt-6 py-5">
    <div class="container-fluid">
      <div class="row">
        <!--Middle Part Start-->
        <div id="" class="col-sm-12">
        <div class="pull-left" style="margin-left: 4%; margin-top: 4%;" >

            <?php
            $order->id = $_REQUEST['order_id'];
            echo '<div class="alert alert-success">
  <strong>'.Yii::t("app","Congratulations!") .'</strong>&nbsp &nbsp Your Order has been Placed Succefully and detail is give below if you have any Query feel free to contact us at info@kistpay.com</div>';
            ?>
            <h1 class="title"><?=Yii::t('app', 'Order')?> #<?=$order->id?> - <?=Yii::t('app', $order->status->label)?></h1></div>

		<div class="pull-right">
		<?php
		if($order->sub_order_status == OrderStatus::_NEW && $order->payment_method != PaymentMethods::_COD)
		{
		?>
		<!--<a href="<?=Url::to(['/order/default/payment', 'order_id' => $order->id])?>" class="btn btn-danger"><?=Yii::t('app', 'Retry Payment')?></a> -->
        <?php
		}
		


		?>
		</div>

        <table class="table table-bordered table-hover">
        <thead>
          <tr>
            <td colspan="3" class="text-left"><?=Yii::t('app', 'Order Details')?></td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td style="width: 33%;" clpull-leftass="text-left">
			  <b><?=Yii::t('app', 'Order ID')?>:</b> No.<?=$order->id?><br>
              <b><?=Yii::t('app', 'Order Date')?>:</b> <?=date('M d, Y, H:i', $order->added_at)?>
			</td>
            <td style="width: 33%;" class="text-left">
			  <b><?=Yii::t('app', 'Payment Method')?>:</b> <?=Yii::t('app', PaymentMethods::find()->where("method='".$order->payment_method."'")->one()->label)?><br>
              <b><?=Yii::t('app', 'Shipping Method')?>:</b> <?=Yii::t('app', 'Flat Rate Shipping')?>
			</td>
			<td style="width: 33%;" class="text-left">
			<?php
            $entity = Yii::$app->user->identity->entity_id;
			$address = multebox\models\Address::find()->where('entity_id='.$entity)->one();//Json::decode($order->address_snapshot);
			$contact =multebox\models\Contact::find()->where('entity_id='.$entity)->one();// Json::decode($order->contact_snapshot);
			?>
			  <b><?=Yii::t('app', 'Shipping Address')?></b><br>
			  <?=$contact['first_name']?> <?=$contact['last_name']?><br>
			  <?=$address['address_1']?><br>
			  <?=$address['address_2']?><br>
			  <?=City::findOne($address['city_id'])->city?><br/>
			  <?=State::findOne($address['state_id'])->state?><br/>
			  <?=Country::findOne($address['country_id'])->country?> - <?=$address['zipcode']?><br/>
			  <?=Yii::t('app', 'Phone')?>: <?=$contact['mobile']?><br/>
			</td>
          </tr>
        </tbody>
      </table>
      
      <div class="table-responsive">
        <table class="table table-bordered table-hover orderitems">
          <thead>
            <tr>
              <td class="text-center"><?=Yii::t('app', 'Image')?></td>
			  <td class="text-left"><?=Yii::t('app', 'Product Name')?></td>
			  <td class="text-right"><?=Yii::t('app', 'Quantity')?></td>
			  <td class="text-right"><?=Yii::t('app', 'Unit Price')?></td>
			  <td class="text-right"><?=Yii::t('app', 'Payment Plan')?></td>

		  	  <td class="text-right"><?=Yii::t('app', 'Total')?></td>
			  <td class="text-right"><?=Yii::t('app', 'Payment Method')?></td>
              <td style="width: 20px;"></td>
            </tr>
          </thead>
          <tbody>
		  <?php
		 // $cart_items = MulteModel::mapJsonArrayToModelArray(Json::decode($order->cart_snapshot), new Cart);
          $cart= $order;
			//$cart = MulteModel::mapJsonToModel($cart, new Cart);
			//var_dump($cart);exit;
              $inventory_item = Inventory::findOne($cart['inventory_id']);
              $prod_title = $inventory_item->product_name;
			$fileDetails = File::find()->where("entity_type='product' and entity_id=".$inventory_item->product_id)->one();
		  ?>
		  <tr>
			<td class="text-center"><img style="height: 53px;" src="<?=Url::base()?>/../../multeback/web/attachments/<?=$fileDetails->id?><?=strrchr($fileDetails->file_name, ".")?>" alt="<?=$prod_title?>" title="<?=$prod_title?>" class="img-thumbnail" /></td>
			<td class="text-left"><?=$prod_title?><br/>
			<td class="text-left">
			  <div class="text-right">
				<?=$cart->total_items?>
			  </div>
			</td>
			<td class="text-right"><?=MulteModel::formatAmount(MulteModel::getInventoryActualPrice($inventory_item) - MulteModel::getInventoryDiscountAmount($inventory_item, $cart->total_items))?></td>
			<td class="text-right"><?= ($cart->pay_process_id) == 1 ? 'Installments': 'Full Cash'?></td>

            <td>
                <?php if($cart->plan_id == Null ||$cart->plan_id == 0){
                    echo MulteModel::getAdvance($cart->total_cost, $cart->plan_id);

                }
                else{
                    echo 'Rs'.$cart->total_cost;
                }?>
            </td>
			<?php
			if($sub_order->sub_order_status == OrderStatus::_CANCELED)
			{
				$label_color = 'label-danger';
			}
			else if($sub_order->sub_order_status == OrderStatus::_RETURNED)
			{
				$label_color = 'label-warning';
			}
			else if($sub_order->sub_order_status == OrderStatus::_REFUNDED)
			{
				$label_color = 'label-success';
			}
			else
			{
				$label_color = 'label-info';
			}
			?>
			<td class="text-right"><span class="label <?=$label_color?>"><?=Yii::t('app', $sub_order->status->label)?></span></td>
			<?php
			if($sub_order->sub_order_status != OrderStatus::_REFUNDED && $sub_order->sub_order_status != OrderStatus::_CANCELED)
			{
				$total_cart_price += MulteModel::getInventoryTotalAmount($inventory_item, $cart->total_items)*$cart->total_items;
			}
			?>
							  
              <td style="white-space: nowrap;" class="text-center">
				<a href="<?=Url::to(['/order/default/information', 'order_id' => $order->id, 'cancel_id' => $sub_order->id])?>" data-toggle="tooltip" title="<?=Yii::t('app', 'Cancel Item')?>" id="cancelitem" onclick="return confirm('<?=Yii::t ('app','Are you Sure!')?>')"><i class="fa fa-times-circle"></i></a>
                <a href="<?=Url::to(['/order/default/information', 'order_id' => $order->id, 'return_id' => $sub_order->id])?>" data-toggle="tooltip" title="<?=Yii::t('app', 'Return Item')?>" id="returnitem" onclick="return confirm('<?=Yii::t ('app','Are you Sure!')?>')"><i class="fa fa-reply"></i></a>

			  </td>

            </tr>
	      <?php


		  $global_discount = $order->total_site_discount; 
		  $coupon_discount = $order->total_coupon_discount;
							
			if ($global_discount > 0)
			{
			?>
			  <tr>
				<input type="hidden" name="special_discount" value="<?=$global_discount?>">
				<td class="text-right" colspan="9"><strong><?=Yii::t('app', 'Total Special Discount')?>:</strong></td>
				<td class="text-right"><?=MulteModel::formatAmount($global_discount)?></td>
			  </tr>
			<?php
			}
			
			if ($coupon_discount > 0)
			{
			?>
			  <tr>
				<input type="hidden" name="coupon_discount" value="<?=$coupon_discount?>">
				<td class="text-right" colspan="9"><strong><?=Yii::t('app', 'Total Coupon Discount')?> (<?=Json::decode($order->discount_coupon_snapshot)['coupon_code']?>):</strong></td>
				<td class="text-right"><?=MulteModel::formatAmount($coupon_discount)?></td>
			  </tr>
			<?php
			}
			?>

			  <tr>
				<input type="hidden" name="coupon_discount" value="0">
				<input type="hidden" name="total_cost" value="<?=$total_cart_price - $global_discount?>">
				<td class="text-right" colspan=9><strong><?=Yii::t('app', 'Total Order Amount')?>:</strong></td>
			  </tr>


          </tbody>
        </table>
      </div>
                 
      <div class="buttons clearfix">
        <div class="pull-right"><a class="btn btn-primary" href="<?=Url::to(['/site/index'])?>"><?=Yii::t('app', 'Continue')?></a></div>
      </div>
              
            
               
        </div>
        <!--Middle Part End -->
      </div>
    </div>
</section>