<?php

namespace multebox;

use Yii;

/**
 * Controller for Mult-e-commerce system.
 */
class Controller extends \yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
       /** Added by TechRaft Solutions for language/timezone change at runtime **/
		Yii::$app->language = Yii::$app->params['LOCALE'];
		Yii::$app->timezone = Yii::$app->params['TIME_ZONE'];
		return true;
    }
}
