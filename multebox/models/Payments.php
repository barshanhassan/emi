<?php

namespace multebox\models;

use Yii;

/**
 * This is the model class for table "payments".
 *
 * @property int $id
 * @property int $customer_id
 * @property int $order_id
 * @property int $sub_order_id
 * @property int $payment_method
 * @property int $plan_id
 * @property string $due_date
 * @property int $installment_no
 * @property int $installment_amonut
 * @property int $other_charges
 * @property int $discount
 * @property int $grand_total
 * @property int $total_paid
 * @property int $paid_by
 * @property string $payment_date
 * @property int $mop
 * @property int $status
 * @property int $refund_reason
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 */
class Payments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id', 'order_id', 'payment_method', 'plan_id', 'installment_no', 'installment_amonut', 'other_charges', 'discount', 'grand_total', 'total_paid', 'paid_by', 'mop', 'status', 'refund_reason', 'created_by', 'updated_by'], 'integer'],
            [['due_date', 'payment_date', 'created_at', 'updated_at'], 'safe'],
            [['created_at', 'created_by'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer ID',
            'order_id' => 'Order ID',
            'payment_method' => 'Payment Method',
            'plan_id' => 'Plan ID',
            'due_date' => 'Due Date',
            'installment_no' => 'Installment No',
            'installment_amonut' => 'Installment Amonut',
            'other_charges' => 'Other Charges',
            'discount' => 'Discount',
            'grand_total' => 'Grand Total',
            'total_paid' => 'Total Paid',
            'paid_by' => 'Paid By',
            'payment_date' => 'Payment Date',
            'mop' => 'Mop',
            'status' => 'Status',
            'refund_reason' => 'Refund Reason',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['username' => 'customer_name']);
    }
}
