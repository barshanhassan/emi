<?php

namespace multebox\modules\multeobjects;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'multebox\modules\multeobjects\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
