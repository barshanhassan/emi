<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var multebox\models\PrdMdl $model
 */

$this->title = $model->mdl_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Prd Mdls'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prd-mdl-view">
	<!--
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
	-->


    <?= DetailView::widget([
            'model' => $model,
            'condensed'=>false,
            'hover'=>true,
            'mode'=>Yii::$app->request->get('edit')=='t' ? DetailView::MODE_EDIT : DetailView::MODE_VIEW,
            'panel'=>[
            'heading'=>$this->title,
            'type'=>DetailView::TYPE_INFO,
        ],
        'attributes' => [
    //        'id',
            'mdl_name',
            'mdl_desc',
            'mdl_status',
      //      'added_at',
     //       'updated_at',
        ],
        'deleteOptions'=>[
        'url'=>['delete', 'id' => $model->id],
        'data'=>[
        'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?'),
        'method'=>'post',
        ],
        ],
        'enableEditMode'=>false,
    ]) ?>

</div>
