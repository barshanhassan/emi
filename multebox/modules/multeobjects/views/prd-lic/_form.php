<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var multebox\models\PrdLic $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="prd-lic-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

    'model' => $model,
    'form' => $form,
    'columns' => 1,
    'attributes' => [

'prd_lic_name'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Prd Lic Name...', 'maxlength'=>255]], 

'prd_lic_desc'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Prd Lic Desc...', 'maxlength'=>1024]], 


 'prd_lic_status' => [ 

										'type' => Form::INPUT_DROPDOWN_LIST,

										'columnOptions'=>['colspan'=>1],

										'items'=>array('0'=> Yii::t('app', 'No') ,'1'=> Yii::t('app', 'Yes'))  , 

										'options' => [ 

                                                'prompt' => '--'.Yii::t('app', 'Select Status').'--'

                                        ]

								],


'prd_lic_date'=>['type'=> Form::INPUT_WIDGET, 'widgetClass'=>DateControl::classname(),'options'=>['type'=>DateControl::FORMAT_DATETIME]], 

//'added_at'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Added At...']], 

//'updated_at'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Updated At...']], 

    ]


    ]);
	if(empty($_GET['id'])){
    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
    
	
	}
	
	ActiveForm::end(); 
	?>

</div>
