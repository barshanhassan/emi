<?php

namespace app\helpers;

use app\models\Activities;
use app\models\PropertyCategory;
use app\models\PropertyPurpose;
use app\models\Settings;
use app\models\User;
use app\modules\rbac\models\AuthAssignment;
use DateTime;
use Yii;
use yii\base\Exception;

/**
 * Created by PhpStorm.
 * User: Waqar
 * Date: 10/28/2017
 * Time: 2:35 PM
 */


class Helper
{

    public static function sendSMS($phone,$message)
    {
        $developer = 'demo';
        if($developer=='live'){


             $url = 'http://pk.eocean.us/APIManagement/API/RequestAPI?user=winwin&pwd=AAbbqFE8NlLCpE7a8ttT2eBZjXE2N6fPzKWimgLqyHzQerIiNpeVjGhTTLotHheP6g%3d%3d&sender=WIN%20WIN&reciever='.urlencode($phone).'&msg-data='.urlencode($message) .'&response=string';

        }else
        {
            $url = 'http://pk.eocean.us/APIManagement/API/RequestAPI?user=maaliksoft&pwd=ACoeTWjUB4lccfc1lIg4MAxqyVLKeskk4gGDcVYRy6CJOOawh59CQgtSTCp4Ov33Jw%3d%3d&sender=MaalikSoft&reciever='.urlencode($phone).'&msg-data='.urlencode($message) .'&response=string';
        }


        $cSession = curl_init();
        curl_setopt($cSession, CURLOPT_URL, $url);
        curl_setopt($cSession, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cSession, CURLOPT_HEADER, false);
        $result = curl_exec($cSession);
        
        curl_close($cSession);


        if (strpos($result, 'Message accepted for delivery') !== false) {
            $status = 'sent';
            return true;

        }
        else if(strpos($result, 'API Execute Successfully') !== false)
        {
            $status = 'sent';
            return true;
        }
        else{
            $status = 'failure';
            return true;
        }

    }


    public static function sendEmail($email,$emailmessage,$emailsubject)
    {
        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL,'http://kistpay.com/email-send/send.php');
        curl_setopt($ch,CURLOPT_POST,1);
        curl_setopt($ch,CURLOPT_POSTFIELDS, "email=".$email."&message=".$emailmessage."&subject=".$emailsubject."");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        //execute post
          $result = curl_exec($ch);
           


    

        //close connection
        curl_close($ch);
        // Further processing ...
        if ($result == "OK")
        {
            return true;
        }
        else
        {
        }
    }


    public  static function getSize($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
    }



    public static function DateTime($dateTime)
    {
        $phpdate = strtotime( $dateTime );
        $date = date( 'd/m/Y', $phpdate );
        $time = date('h:i A',$phpdate);

        return $date.'<br>'.$time;
    }

    public static function getCreated($id)
    {
        $user = User::findOne($id);

        return  $user->username;
    }




    public static function getBaseUrl()
    {
        $url = Yii::$app->homeUrl;
        $str  = str_replace("/web","",$url);

        return $str;
    }


    public static function DatTim($dateTime)
    {
        $phpdate = strtotime($dateTime);
        $date = date('d/m/Y h:i A', $phpdate);
        return $date;
    }

    public static function getMySql($dateTime)
    {
        $phpdate = strtotime($dateTime);
        $date = date('d/m/Y h:i A', $phpdate);


        return $date;
    }

    public static function getUser($id)
    {
        $user = User::findOne($id);
        return $user->username;
    }

}
?>