<?php

namespace multeback\modules\banks\controllers;

use Yii;
use multebox\models\Banks;
use multebox\models\search\BanksSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * BanksController implements the CRUD actions for Banks model.
 */
class BanksController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Banks models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BanksSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Banks model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Banks model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Banks();

        if ($model->load(Yii::$app->request->post())){

            $model->created_by = Yii::$app->user->identity->getId();
            $model->created_at = date("Y-m-d H:i:s");
            $model->logo = UploadedFile::getInstances($model, 'logo');
            if($model->logo)
            {
                $directory = Yii::getAlias('@app/drive/banklogo') . DIRECTORY_SEPARATOR;
                if (!is_dir($directory)) {
                    FileHelper::createDirectory($directory);
                } else
                    foreach ($model->logo as $key => $file) {
                        $timestamp1 = date('YmdHis');
                        $ext = pathinfo($file->name, PATHINFO_EXTENSION);
                        $filename = $model->name.'-'.$timestamp1.$key.'.'.$ext;
                        $file_name1 = str_replace(array(" ", "&"), array("_", "-"), $filename);
                        $filePath = $directory . $file_name1;
                        $model->logo = $file_name1;

                        if ($file->saveAs($filePath)) {


                        }
                    }
            }
            if($model->save()){
                return $this->redirect(['index']);
            }

        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Banks model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())){

            $model->created_by = Yii::$app->user->identity->getId();
            $model->created_at = date("Y-m-d H:i:s");
            $model->logo = UploadedFile::getInstances($model, 'logo');
            if($model->logo)
            {
                $directory = Yii::getAlias('@app/drive/banklogo') . DIRECTORY_SEPARATOR;
                if (!is_dir($directory)) {
                    FileHelper::createDirectory($directory);
                } else
                    foreach ($model->logo as $key => $file) {
                        $timestamp1 = date('YmdHis');
                        $ext = pathinfo($file->name, PATHINFO_EXTENSION);
                        $filename = $model->name.'-'.$timestamp1.$key.'.'.$ext;
                        $file_name1 = str_replace(array(" ", "&"), array("_", "-"), $filename);
                        $filePath = $directory . $file_name1;
                        $model->logo = $file_name1;

                        if ($file->saveAs($filePath)) {


                        }
                    }

            }
            if($model->save()){
                return $this->redirect(['index']);
            }

        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Banks model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Banks model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Banks the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Banks::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
