<?php

namespace multeback\modules\banks;

/**
 * bank module definition class
 */
class Banks extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'multeback\modules\banks\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
