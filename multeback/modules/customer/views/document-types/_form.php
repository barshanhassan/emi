<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var multebox\models\DocumentTypes $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="document-types-form">
    <?php $form = ActiveForm::begin(['id'=>'form']); ?>
    <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo Yii::t ( 'app', 'Document Types' ); ?></h3>
        </div>
        <div class="panel-body">
            <?php $form = ActiveForm::begin(['id'=>'form']); ?>

            <?= $form->field($model, 'name')->textInput(['class'=>'form-control rounded']) ?>

            <?= $form->field($model, 'status')->dropDownList(['1'=>'Active','0'=>'Inactive'],['prompt'=>'Select......','class'=>'form-control rounded']) ?>


            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>


        </div>
    </div>

    <?php ActiveForm::end(); ?>


</div>
